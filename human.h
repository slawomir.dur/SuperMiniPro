#ifndef HUMAN_H
#define HUMAN_H

#include <iostream>
#include <memory>
using std::string;

class Human
{
	protected:
		string firstname;
		string surname;
	public:
		explicit Human (const string& fname = "NULL", const string& sname = "NULL") : firstname(fname), surname(sname) {};
		virtual ~Human() {};
		string& Firstname() {return firstname;};
		const string& Firstname() const {return firstname;};
		string& Surname() {return surname;};
		const string& Surname() const {return surname;};
		void sayName() const { std::cout << "My name is " << firstname << std::endl; };
		virtual void sayReversedName() const = 0;
		void show() const {std::cout << "Imie: " << firstname << std::endl << "Nazwisko: " << surname << std::endl;};
		friend std::ostream& operator<<(std::ostream& os, const Human& h);
};

class Man : public Human
{
	public:
		Man(const string& fname = "NULL", const string& sname = "NULL") : Human(fname, sname) {};
		virtual ~Man() {};
		void sayReversedName() const override;
		friend std::ostream& operator<<(std::ostream& os, const Man& h);
};

class Woman : public Human
{
	public:
		Woman(const string& fname = "NULL", const string& sname = "NULL") : Human(fname, sname) {};
		virtual ~Woman() {};
		void sayReversedName() const override;
		friend std::ostream& operator<<(std::ostream& os, const Woman& h);
};


class Family
{
    public:
        class FamilyFactory
        {
        public:
            FamilyFactory() {};
            std::unique_ptr<Family> createFamily(const string &nameOfHusband, const string &surname, const string &nameOfWife)
                {return std::unique_ptr<Family>(new Family(nameOfHusband, nameOfWife, surname));};
            std::unique_ptr<Family> createFamily(const Human &man, const Human &woman)
                {return std::unique_ptr<Family>(new Family(man.Firstname(), woman.Firstname(), man.Surname()));};
        };
 
    private:
        class Husband : public Man
        {
        private:
            string nameOfWife;
        public:
            Husband(const string &name_, const string &surname_, const string &nameOfWife_)
                : Man(name_, surname_), nameOfWife(nameOfWife_) {};
            ~Husband() {};
        };
 
        class Wife : public Woman
        {
        private:
            string nameOfHusband;
        public:
            Wife(const string &name_, const string &surname_, const string &nameOfHusband_)
                : Woman(name_, surname_), nameOfHusband(nameOfHusband_) {};
            ~Wife() {};
        };
 
    public:
        enum sex {MAN, WOMAN};
    private:
        Husband husband;
        Wife wife;
        int numOfChildren;
    public:
        Family(const string &nameOfHusband, const string &nameOfWife, const string &surname)
            : husband(nameOfHusband, surname, nameOfWife), wife(nameOfWife, surname, nameOfHusband), numOfChildren(0) {};
        void show();
        std::unique_ptr<Human> makeChild(Family::sex sex_, const string &name);
        std::unique_ptr<Human> divorce();
        friend void exchangeHusbands(Family &f1, Family &f2);
};

#endif // HUMAN_H