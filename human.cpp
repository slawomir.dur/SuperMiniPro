#include "human.h"
#include <iterator>
#include <algorithm>

std::ostream& operator<<(std::ostream& os, const Human& h)
{
	os << "Name: " << h.firstname << std::endl <<
		  "Surname: " << h.surname << std::endl;
}

void Man::sayReversedName() const
{
	std::ostream_iterator<char> out(std::cout);
	std::cout << "My reversed name is: ";
	std::copy(firstname.rbegin(), firstname.rend(), out);
}
	


std::ostream& operator<<(std::ostream& os, const Man& h)
{
	os << static_cast<const Human&>(h);
	return os;
}

void Woman::sayReversedName() const
{
	std::ostream_iterator<char> out(std::cout);
	std::cout << "My reversed name is: ";
	std::copy(firstname.rbegin(), firstname.rend(), out);
}

std::ostream& operator<<(std::ostream& os, const Woman& h)
{
	os << static_cast<const Human&>(h);
	return os;
}

std::unique_ptr<Human> Family::makeChild(Family::sex sex_, const string &name)
{
	numOfChildren++;
	if (sex_ == sex::MAN)
		return std::unique_ptr<Human>(new Man(name, husband.Surname()));
	else if (sex_ == sex::WOMAN)
		return std::unique_ptr<Human>(new Woman(name, husband.Surname()));
	else
	{
		numOfChildren--;
		return nullptr;
	}
}

void Family::show()
{
    std::cout << "Rodzina: " << husband.Surname() << std::endl
         << "Imie meza: " << husband.Firstname() << std::endl
         << "Imie zony: " << wife.Firstname() << std::endl
         << "Liczba dzieci: " << numOfChildren << std::endl;
}

std::unique_ptr<Human> Family::divorce()
{
    std::unique_ptr<Human> temp(new Man(husband.Firstname(), husband.Surname()));
    husband.Firstname() = "NULL";
    return std::unique_ptr<Human>(new Man(temp->Firstname(), temp->Surname()));
}

void exchangeHusbands(Family &f1, Family &f2)
{
    Family::Husband temp = f2.husband;
    f2.husband = f1.husband;
    f1.husband = temp;
}