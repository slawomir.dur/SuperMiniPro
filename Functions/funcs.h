#include <iostream>
using std::string;

void print_reversed(const string& text);

void print_mingled(const string& text);

void print_all_mingled_versions(const string& text);