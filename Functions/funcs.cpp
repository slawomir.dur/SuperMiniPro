#include "funcs.h"
#include <algorithm>
#include <iterator>
#include <iostream>

using std::string;

void print_reversed(const string& text)
{
	std::copy(text.rbegin(), text.rend(), std::ostream_iterator<char>(std::cout));
	return;
}

void print_mingled(const string& text)
{
	string text2 = text;
	std::random_shuffle(text2.begin(), text2.end());
	std::cout << text2 << std::endl;
}

void print_all_mingled_versions(const string& text)
{
	string text2 = text;
	std::sort(text2.begin(), text2.end());
	do
	{
		std::cout << text2 << std::endl;
	} while (std::next_permutation(text2.begin(), text2.end()));
}
