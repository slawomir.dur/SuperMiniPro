#include "human.h"
#include <iostream>
#include <memory>
#include "FamilyConfig.h"
#include "funcs.h"


int main(int argc, char **argv)
{
	using std::cout;
	using std::cin;
	using std::endl;
	using std::unique_ptr;
	
	std::cout << "VERSION " << Family_VERSION_MAJOR << " " << Family_VERSION_MINOR << std::endl;
	
	std::cout << "\nTest of Functions from funcs.h:\n\n";
	std::cout << argv[1] << ": "; 
	print_reversed(argv[1]);
	std::cout << std::endl;
	print_mingled(argv[1]);
	print_all_mingled_versions(argv[1]);
	
	std::cout << "\n\n\nTest of human.h class:\n\n";
	
	Family::FamilyFactory factory;
    Family f1("Hubert", "Kasia", "Stefczyk");
    f1.show();
    cout << endl;
    Family f2("Waldemar", "Jolanta", "Krawczynscy");
    f2.show();
    cout << endl;
 
 
 
    cout << "Hubert i Kasia robia dziecko.\nDziecko:\n";
 
    unique_ptr<Human> child(f1.makeChild(Family::MAN, "Jakub"));
    child->show();
    cout << endl << endl;;
 
    cout << "Waldemar i Jolanta tez robia dziecko.\nDziecko:\n";
    unique_ptr<Human> child2(f2.makeChild(Family::WOMAN, "Paulina"));
    child2->show();
    cout << endl << endl;
 
    cout << "Jakub i Paulina sie poznaja i tworza rodzine!\n";
    unique_ptr<Family> newFamily(factory.createFamily(*child, *child2));
    newFamily->show();
    cout << endl << endl;
 
    cout << "Pierwsze rodziny zamieniaja mezow:\n";
    exchangeHusbands(f1, f2);
    f1.show();
    cout << endl;
    f2.show();
    cout << endl;
    cout << endl;
 
    cout << "Oczywiscie to skonczylo sie rozwodami:\n";
    unique_ptr<Human> divorcee(f1.divorce());
    cout << "Rozwodnik:\n";
    divorcee->show();
    cout << endl;
    cout << "Rodzina po rozwodzie:\n";
    f1.show();
 
    return 0;
	
	return 0;
}